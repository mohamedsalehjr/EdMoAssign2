
(1) How Everything Was Implemented

	1.a)The Board
The board was implemented by creating a 2D array. 

	1.b)The Discs
The discs were placed on the board using an if statement.
To create the graphic of the board, for the lack of a better word, for loops were used and 
in these for loops were two if statements that placed our starting 4 counters in the middle. 

	1.c)Players
The players and their discs were created using a struct.
We placed a struct into a different library "player.h" which
contains the players and their disc type. This was easier than having to create variables specific to
each player.


(2) How Work Was Divided

I came up with a checklist of what we would have to do.
Mohamed's role was to create the player struct whereas my role was place the counters
on the board. The former came to be easier than the later as Mohamed was able to create the struct
and put it into a new library on his IDE. He helped me with my role as an issue I ran into was
that I was able to declare a point on the board to be a value (1 or 0) but I was unable to place it inside the squares.
Mohamed placed a new square with a value but that square pushed the row it was on to the right. 
I was able to solve the problem with an else statement then I completed placing the rest of the counters on the board. 
Afterwards Mohamed tided up the code and I added the comments. 