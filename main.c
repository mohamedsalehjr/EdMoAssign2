#include <stdio.h>
#include "player.h"
#define BOARD_SIZE 8 //Board size declared as a m

int main(void){

int row; //Rows and columns
int col;

struct player player1; //Our players and their type are
struct player player2; // declared here.

player1.disktype = 1;
player2.disktype = 0;

puts("Enter name of player 1"); //Users enter names of players 1 and 2.
gets(player1.playername);

puts("Enter name of player 2");
gets(player2.playername);

printf("%s 's disc is: 1 \t %s 's is: 0", player1.playername,player2.playername);

int board[BOARD_SIZE][BOARD_SIZE]; //Board is declared.


printf("\n ");
for(row = 0 ; row < BOARD_SIZE; row++) //Displays top line numbers
	{
		printf("  %d\t", row);
	}
    printf("\n");


    for(row = 0; row < BOARD_SIZE; row++)
    {
		printf("%d|", row); //Prints rows
        for(col = 0; col < 8; col++)//Prints columns
        {
					if ((col == 3 && row == 3) || (col == 4 && row == 4)){ //The two if statemnts place initial four counters on board.
						printf("[1] \t");
					    }
							else if ((col == 4 && row == 3) || (col == 3 && row == 4)){
								printf("[0] \t");
							    }
							    else{
							        printf("[ ] \t"); //Prints rest of squares.

							    }
        }
        printf("\n");
    }

return 0;
}


